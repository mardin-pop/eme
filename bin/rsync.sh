#!/usr/bin/env bash
# Color
red='\033[0;31m'          # Red
green='\033[0;32m'        # Green
yellow='\033[0;33m'       # Yellow
# dotfile folder
eme="${HOME}/Documents/git/eme/"
dot_init="$HOME/.emacs"
eme_init="${eme}/src/.emacs"

function banner(){
    echo -e "    ${geen}<--${yellow} Update and Sync Dofile${green} -->"
}

function syncer_func(){
  [ -e $1 ] && cp -r $1 $2 ;echo -e "${green}[*] ${yellow} $3 done..." \
            || echo -e "${red}[!] ${yellow}$3 Configuration file not found"
}
function ctrl_c(){
  echo -e "\n\n${RED}Quitting...${RESET}\n"
  exit 2
}

function push_do(){
  {
    git -C $eme add -A;
    git -C $eme commit -m "All config file updated" ;
  } > /dev/null
  git -C $eme push
  #printf "%s" "Do you want to push the changes(y,n):"
  # read ans
  # if [[ $ans -eq "y" ]];then
  #     git -C $eme push
  # else
  #     printf "${yellow}[!] Nothing pushed."
  # fi
}

main(){
      clear
      banner
      trap ctrl_c INT
      syncer_func $dot_init $eme_init "init Emacs"    
      push_do
};main
