; Add melpa repo
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;Disable the start splesh screen
(setq inhibit-startup-screen t)

; Disable the menu bar
(menu-bar-mode 0)

; Disable the tool bar
(tool-bar-mode 0)

; Disable the scroll bar
(scroll-bar-mode -1)

; Font: Fira Code
(set-default-font "Fira Code-9")

; Visual select background color
(set-face-attribute 'region nil :background "#666")

; ido mode & vertical mode
(ido-mode)
(require 'ido-vertical-mode)
(ido-mode 1)
(ido-vertical-mode 1)
(setq ido-vertical-define-keys 'C-n-and-C-p-only)

; Do not ask for theme threat
(setq custom-safe-themes t)

; Backup from main config file
;(setq backup-directory-alist '(("." . "~/.emacs_saves")))
; Disable the auto indent
(when (fboundp 'electric-indent-mode) (electric-indent-mode -1))



; Show line number
(global-linum-mode t)
(setq linum-format "%3d \u2503")
(setq line-number-mode t)
(setq column-number-mode t)
(setq doc-view-continuous t)

; Powerline
(display-time-mode)
(require 'smart-mode-line)

(set-face-attribute 'mode-line nil
                    :foreground "Black"
                    :background "#000000"
                    :box nil)
(setq powerline-arrow-shape 'curve)
(setq powerline-default-separator-dir '(right . left))
(setq sml/theme 'powerline)
(setq sml/mode-width 0)
(setq sml/name-width 20)
(rich-minority-mode 1)
(setf rm-blacklist "")
(sml/setup)
(use-package all-the-icons)
; Keyword used in todo file and org-mode
(setq org-todo-keywords
  '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))

; Shift selection
;(setq shift-select-mode t)

; Change cursor block to |-Bleam
(setq-default cursor-type 'bar) 

	;------------------------------------------------;
        ;               Keyboard shortcut                ;
	;------------------------------------------------;
		   ; C-S-up   : swap line to up
		   ; C-S-down : swap line to down
		   ;       F8 : Neotree toggle
(defun forward-symbol-shift-aware (arg)
  "`forward-symbol', with shift-select-mode support.
Shift + this command's key extends/activates the region
around the text moved over."
  (interactive "^p")
  (forward-symbol arg))

(local-set-key (kbd "C-<right>") 'forward-symbol-shift-aware)
(local-set-key (kbd "C-<left>") (lambda () (interactive "^")
                                  (forward-symbol-shift-aware -1)))

(defun move-text-internal (arg)
   (cond
    ((and mark-active transient-mark-mode)
     (if (> (point) (mark))
            (exchange-point-and-mark))
     (let ((column (current-column))
              (text (delete-and-extract-region (point) (mark))))
       (forward-line arg)
       (move-to-column column t)
       (set-mark (point))
       (insert text)
       (exchange-point-and-mark)
       (setq deactivate-mark nil)))
    (t
     (beginning-of-line)
     (when (or (> arg 0) (not (bobp)))
       (forward-line)
       (when (or (< arg 0) (not (eobp)))
            (transpose-lines arg))
       (forward-line -1)))))

(defun move-text-down (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines down."
   (interactive "*p")
   (move-text-internal arg))

(defun move-text-up (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines up."
   (interactive "*p")
   (move-text-internal (- arg)))

(global-set-key [\C-\S-up] 'move-text-up)
(global-set-key [\C-\S-down] 'move-text-down)

(windmove-default-keybindings 'meta)

(global-set-key (kbd "<M-S-up>") 'shrink-window)
(global-set-key (kbd "<M-S-down>") 'enlarge-window)
(global-set-key (kbd "<M-S-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<M-S-right>") 'enlarge-window-horizontally)

	;------------------------------------------------;
        ;                  C/C++ mode                    ;
	;------------------------------------------------;

; Auto pair
(defun custom-c-mode-auto-pair ()
(push ?{
	  (getf autopair-dont-pair :everywhere))
(interactive)
(make-local-variable 'skeleton-pair-alist)
(setq skeleton-pair-alist  '((?{ \n > _ \n ?} >)))
(setq skeleton-pair t)
(local-set-key (kbd "{") 'skeleton-pair-insert-maybe)
)

; Custom C style
(defconst custom-c-style
  '((c-backslash-column . 80) 
    (c-backslash-max-column . 80)
    (c-offsets-alist .
		     ((inextern-lang . 0)
		      (substatement . 0)
		      (statement-case-intro . 0)
		      (substatement-open . 0)
		      (case-label . +)
		      (block-open . 0))))
  "Custom C programming style"
  )
(c-add-style "Custom" custom-c-style)
(defun custom-c-mode-common-hook ()
  (c-set-style "Custom") ; Custom C-Style
  (setq c-basic-offet 4) ; Tab size
  (c-toggle-hungry-state 1) ; Hungry delete
;  (custom-c-mode-auto-pair) ; Auto-pair hook
)
(add-hook 'c-mode-common-hook 'custom-c-mode-common-hook)


; Auto complete
(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)
; Yasnippet
(require 'yasnippet)
(yas-global-mode 1)

;; (defun my:ac-c-header-init()
;;   (require 'auto-complete-c-header)
;;   (add-to-list 'ac-sources 'ac-source-c-header))
;; (add-hook 'c++-mode-hook 'my:ac-c-heaeder-init)
;; (add-hook 'c-mode-hook 'my:ac-c-heaeder-init)

	;------------------------------------------------;
        ;                 Python mode                    ;
	;------------------------------------------------;
; Soon

	;------------------------------------------------;
        ;                Assembly mode                   ;
	;------------------------------------------------;
;; (defun my-asm-mode-hook ()
;;   (local-unset-key (vector asm-comment-char))
;;   (setq tab-always-indent 8))

;; (add-hook 'asm-mode-hook #'my-asm-mode-hook)

	;------------------------------------------------;
        ;                 LaTeX mode                     ;
	;------------------------------------------------;


;------------------------------------------------------------------

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (doom-molokai)))
 '(custom-safe-themes
   (quote
    ("361f5a2bc2a7d7387b442b2570b0ef35198442b38c2812bf3c70e1e091771d1a" "d74c5485d42ca4b7f3092e50db687600d0e16006d8fa335c69cf4f379dbd0eee" "be9645aaa8c11f76a10bcf36aaf83f54f4587ced1b9b679b55639c87404e2499" "11e57648ab04915568e558b77541d0e94e69d09c9c54c06075938b6abc0189d8" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" default)))
 '(org-agenda-files (quote ("~/dotfile/.todo.org")))
 '(package-selected-packages
   (quote
    (google-this google-translate all-the-icons doom-themes neotree fira-code-mode w3m ido-sort-mtime pdf-tools yasnippet molokai-theme chess auto-complete-clang auto-complete-c-headers)))
 '(sml/mode-width
   (if
       (eq
	(powerline-current-separator)
	(quote arrow))
       (quote right)
     (quote full)))
 '(sml/pos-id-separator
   (quote
    (""
     (:propertize " " face powerline-active1)
     (:eval
      (propertize " "
		  (quote display)
		  (funcall
		   (intern
		    (format "powerline-%s-%s"
			    (powerline-current-separator)
			    (car powerline-default-separator-dir)))
		   (quote powerline-active1)
		   (quote powerline-active2))))
     (:propertize " " face powerline-active2))))
 '(sml/pos-minor-modes-separator
   (quote
    (""
     (:propertize " " face powerline-active1)
     (:eval
      (propertize " "
		  (quote display)
		  (funcall
		   (intern
		    (format "powerline-%s-%s"
			    (powerline-current-separator)
			    (cdr powerline-default-separator-dir)))
		   (quote powerline-active1)
		   (quote sml/global))))
     (:propertize " " face sml/global))))
 '(sml/pre-id-separator
   (quote
    (""
     (:propertize " " face sml/global)
     (:eval
      (propertize " "
		  (quote display)
		  (funcall
		   (intern
		    (format "powerline-%s-%s"
			    (powerline-current-separator)
			    (car powerline-default-separator-dir)))
		   (quote sml/global)
		   (quote powerline-active1))))
     (:propertize " " face powerline-active1))))
 '(sml/pre-minor-modes-separator
   (quote
    (""
     (:propertize " " face powerline-active2)
     (:eval
      (propertize " "
		  (quote display)
		  (funcall
		   (intern
		    (format "powerline-%s-%s"
			    (powerline-current-separator)
			    (cdr powerline-default-separator-dir)))
		   (quote powerline-active2)
		   (quote powerline-active1))))
     (:propertize " " face powerline-active1))))
 '(sml/pre-modes-separator (propertize " " (quote face) (quote sml/modes))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Startup file
(find-file "~/dotfile/.todo.org")

(set-fontset-font t 'unicode (font-spec :family "all-the-icons") nil 'append)
(set-fontset-font t 'unicode (font-spec :family "file-icons") nil 'append)
(set-fontset-font t 'unicode (font-spec :family "Material Icons") nil 'append)
(set-fontset-font t 'unicode (font-spec :family "github-octicons") nil 'append)
(set-fontset-font t 'unicode (font-spec :family "FontAwesome") nil 'append)
(set-fontset-font t 'unicode (font-spec :family "Weather Icons") nil 'append)
;-------------------------------
(global-set-key [f8] 'neotree-toggle)

